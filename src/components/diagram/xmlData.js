export const xmlStr = `
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bioc="http://bpmn.io/schema/bpmn/biocolor/1.0" xmlns:color="http://www.omg.org/spec/BPMN/non-normative/color/1.0" id="sid-38422fae-e03e-43a3-bef4-bd33b32041b2" targetNamespace="http://bpmn.io/bpmn" exporter="bpmn-js (https://demo.bpmn.io)" exporterVersion="5.1.2">
  <process id="leave-flow" name="Leave Flow" isExecutable="false">
    <startEvent id="start" name="Start">
      <outgoing>SequenceFlow_0h21x7r</outgoing>
    </startEvent>
    <sequenceFlow id="SequenceFlow_0h21x7r" sourceRef="start" targetRef="Task_1hcentk" />
    <userTask id="Task_1hcentk" name="Fill Apply">
      <incoming>SequenceFlow_0h21x7r</incoming>
      <incoming>Flow_1qegehi</incoming>
      <incoming>Flow_186nq7h</incoming>
      <outgoing>Flow_13b03b0</outgoing>
    </userTask>
    <sequenceFlow id="Flow_13b03b0" name="submit" sourceRef="Task_1hcentk" targetRef="Activity_092k7i8" />
    <exclusiveGateway id="Gateway_0mfhysa">
      <incoming>Flow_11787hy</incoming>
      <outgoing>Flow_05qokkb</outgoing>
      <outgoing>Flow_1qegehi</outgoing>
    </exclusiveGateway>
    <sequenceFlow id="Flow_11787hy" sourceRef="Activity_092k7i8" targetRef="Gateway_0mfhysa" />
    <sequenceFlow id="Flow_05qokkb" sourceRef="Gateway_0mfhysa" targetRef="Activity_0igv4v1" />
    <exclusiveGateway id="Gateway_1il13x2">
      <incoming>Flow_0mzjv8z</incoming>
      <outgoing>Flow_0p2way7</outgoing>
      <outgoing>Flow_186nq7h</outgoing>
    </exclusiveGateway>
    <sequenceFlow id="Flow_0mzjv8z" sourceRef="Activity_0igv4v1" targetRef="Gateway_1il13x2" />
    <sequenceFlow id="Flow_0p2way7" sourceRef="Gateway_1il13x2" targetRef="call-leave-service" />
    <endEvent id="Event_0eyzrvf">
      <incoming>Flow_0tb9gea</incoming>
    </endEvent>
    <sequenceFlow id="Flow_0tb9gea" sourceRef="call-leave-service" targetRef="Event_0eyzrvf" />
    <userTask id="Activity_092k7i8" name="BM approve">
      <incoming>Flow_13b03b0</incoming>
      <outgoing>Flow_11787hy</outgoing>
    </userTask>
    <userTask id="Activity_0igv4v1" name="RM approve">
      <incoming>Flow_05qokkb</incoming>
      <outgoing>Flow_0mzjv8z</outgoing>
    </userTask>
    <serviceTask id="call-leave-service" name="call leave service">
      <incoming>Flow_0p2way7</incoming>
      <outgoing>Flow_0tb9gea</outgoing>
    </serviceTask>
    <sequenceFlow id="Flow_1qegehi" name="back" sourceRef="Gateway_0mfhysa" targetRef="Task_1hcentk" />
    <sequenceFlow id="Flow_186nq7h" name="back" sourceRef="Gateway_1il13x2" targetRef="Task_1hcentk" />
  </process>
  <bpmndi:BPMNDiagram id="BpmnDiagram_1">
    <bpmndi:BPMNPlane id="BpmnPlane_1" bpmnElement="leave-flow">
      <bpmndi:BPMNEdge id="Flow_186nq7h_di" bpmnElement="Flow_186nq7h">
        <omgdi:waypoint x="570" y="135" />
        <omgdi:waypoint x="570" y="190" />
        <omgdi:waypoint x="-30" y="190" />
        <omgdi:waypoint x="-30" y="150" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="268" y="173" width="24" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_1qegehi_di" bpmnElement="Flow_1qegehi">
        <omgdi:waypoint x="290" y="85" />
        <omgdi:waypoint x="290" y="30" />
        <omgdi:waypoint x="-30" y="30" />
        <omgdi:waypoint x="-30" y="70" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="118" y="12" width="24" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0tb9gea_di" bpmnElement="Flow_0tb9gea">
        <omgdi:waypoint x="760" y="110" />
        <omgdi:waypoint x="832" y="110" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0p2way7_di" bpmnElement="Flow_0p2way7">
        <omgdi:waypoint x="595" y="110" />
        <omgdi:waypoint x="660" y="110" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0mzjv8z_di" bpmnElement="Flow_0mzjv8z">
        <omgdi:waypoint x="480" y="110" />
        <omgdi:waypoint x="545" y="110" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_05qokkb_di" bpmnElement="Flow_05qokkb">
        <omgdi:waypoint x="315" y="110" />
        <omgdi:waypoint x="380" y="110" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_11787hy_di" bpmnElement="Flow_11787hy">
        <omgdi:waypoint x="200" y="110" />
        <omgdi:waypoint x="265" y="110" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_13b03b0_di" bpmnElement="Flow_13b03b0">
        <omgdi:waypoint x="20" y="110" />
        <omgdi:waypoint x="100" y="110" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="44" y="92" width="33" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0h21x7r_di" bpmnElement="SequenceFlow_0h21x7r">
        <omgdi:waypoint x="-142" y="110" />
        <omgdi:waypoint x="-80" y="110" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="StartEvent_1y45yut_di" bpmnElement="start" bioc:stroke="#1e88e5" bioc:fill="#bbdefb" color:background-color="#bbdefb" color:border-color="#1e88e5">
        <omgdc:Bounds x="-178" y="92" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="-172" y="135" width="24" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1re9pu5_di" bpmnElement="Task_1hcentk">
        <omgdc:Bounds x="-80" y="70" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Gateway_0mfhysa_di" bpmnElement="Gateway_0mfhysa" isMarkerVisible="true">
        <omgdc:Bounds x="265" y="85" width="50" height="50" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Gateway_1il13x2_di" bpmnElement="Gateway_1il13x2" isMarkerVisible="true">
        <omgdc:Bounds x="545" y="85" width="50" height="50" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Event_0eyzrvf_di" bpmnElement="Event_0eyzrvf" bioc:stroke="#43a047" bioc:fill="#c8e6c9" color:background-color="#c8e6c9" color:border-color="#43a047">
        <omgdc:Bounds x="832" y="92" width="36" height="36" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1bcjwmn_di" bpmnElement="Activity_092k7i8">
        <omgdc:Bounds x="100" y="70" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_00yxeh6_di" bpmnElement="Activity_0igv4v1">
        <omgdc:Bounds x="380" y="70" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1cfvbyj_di" bpmnElement="call-leave-service">
        <omgdc:Bounds x="660" y="70" width="100" height="80" />
      </bpmndi:BPMNShape>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</definitions>

`
